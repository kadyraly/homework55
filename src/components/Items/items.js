import React from 'react';
import Squer from '../Squer/Squer';

const  Items  = props => {
    let itemStyle = {background: 'white', border: '2px solid black', width: "400px", height: '400px'};
    return (
        <div style = {itemStyle}>
            {props.squers.map((squer, index) => <Squer isClicked={squer.isClicked} key={index} click={() => props.click(index)} hasItem={squer.hasItem} />)}
        </div>
    );
};

export default Items;