import React from 'react';
import './Squer.css';

const  Squer  = props => {
    let squerStyle = {background: 'black', width: '50px', height: '50px', display: 'inline-block', margin: '5px', position: 'relative'};

    if (props.isClicked) {
        squerStyle.background = 'white';
    }

    return (
        <div style ={squerStyle} onClick={props.click}>
            {props.hasItem ? <div className="hideElement">o</div> : null}
        </div>
    );




};

export default Squer;