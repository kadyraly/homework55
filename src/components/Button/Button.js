import React from 'react';

const  Button  = props => {
    return (
        <button className='btn' onClick={props.reset}>Reset</button>
    )
};

export default Button;