import React, { Component } from 'react';

import './App.css';
import Button from './components/Button/Button';
import Counter from './components/Counter/counter';
import Items from "./components/Items/items";


class App extends Component {
    state = {
        items:[],
        counter: 0
    };

    componentDidMount = () => {
        const items = this.generateItems();
        this.setState({items});
    }

    generateItems = () => {
       const item = [];
       for (let i = 0; i < 36; i++) {
           const squer = {hasItem: false, isClicked: false};
           item.push(squer);
       }

       item[Math.floor(Math.random() * 36)].hasItem = true;

       return item;
    }

    onClick = (index) => {
        let items = [...this.state.items];
        let item = {...this.state.items[index]};
        item.isClicked = true;
        items[index] = item;
        let counter = this.state.counter;
        counter++
        this.setState({items, counter});

    }

    getReset = () => {
        this.setState({items: this.generateItems(), counter: 0});
    };


  render() {
    return (
      <div className="App">
        <Items squers={this.state.items} click={this.onClick} />
        <Counter counter={this.state.counter} />
        <Button reset={this.getReset}/>
      </div>
    );
  }
}

export default App;
